
# Lekcja 1 - Markdown lekki język znaczników  
  
##  1. <a name='Spistreci'></a>Spis treści
<!-- vscode-markdown-toc -->
* 1. [Spis treści.........................................................................................................................................1](#Spistreci)
* 2. [Wstęp................................................................................................................................................1](#Wstp)
* 3. [Podstawy składni...........................................................................................................................3](#Podstawyskadni)
* 4. [Definiowanie nagłówków...........................................................................................................3](#Definiowanienagwkw)
* 5. [Definiowanie list.............................................................................................................................4](#Definiowanielist)
* 6. [Wyróżnianie tekstu........................................................................................................................4](#Wyrnianietekstu)
* 7. [Tabele..................................................................................................................................................5](#Tabele)
* 8. [Odnośniki do zasobów................................................................................................................5](#Odnonikidozasobw)
* 9. [Obrazki...............................................................................................................................................5](#Obrazki)
* 10. [Kod źródłowy dla różnych języków programowania.......................................................5](#Kodrdowydlarnychjzykwprogramowania)
* 11. [Tworzenie spisu treści na podstawie nagłówków..............................................................6](#Tworzeniespisutrecinapodstawienagwkw)
* 12. [Edytory dedykowane.....................................................................................................................7](#Edytorydedykowane)
* 13. [Pandoc – system do konwersji dokumentów Markdown do innych formatów.....8](#PandocsystemdokonwersjidokumentwMarkdowndoinnychformatw)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->

* * *

##  2. <a name='Wstp'></a>Wstęp  
Obecnie powszechnie wykorzystuje się języki ze znacznikami do opisania dodatkowych informacji  
umieszczanych w plikach tekstowych. Z pośród najbardziej popularnych można wspomnieć o:  
1. **html** – służącym do opisu struktury informacji zawartych na stronach internetowych,  
2. **Tex** (Latex) – poznany na zajęciach język do „profesjonalnego” składania tekstów,  
3. **XML**  (*Extensible Markup Language*) - uniwersalnym języku znaczników przeznaczonym  
do reprezentowania różnych danych w ustrukturalizowany sposób.  
  
Przykład kodu html i jego interpretacja w przeglądarce:  
```html
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>Przykład</title>
</head>
<body>
<p> Jakiś paragraf tekstu</p>
</body>
</html>
``` 
![zdj1](./1zdj.JPG )  
Przykład kodu *Latex* i wygenerowanego pliku w formacie pdf  
```LaTeX
\documentclass[]{letter}
\usepackage{lipsum}
\usepackage{polyglossia}
\setmainlanguage{polish}
\begin{document}
\begin{letter}{Szanowny Panie XY}
\address{Adres do korespondencji}
\opening{}
\lipsum[2]
\signature{Nadawca}
\closing{Pozdrawiam}
\end{letter}
\end{document}
```
![zdj2](./2zdj.JPG)  

Przykład kodu XML – fragment dokumentu SVG (Scalar Vector Graphics)  
```html
<!DOCTYPE html>
<html>
<body>
<svg height="100" width="100">
 <circle cx="50" cy="50" r="40" stroke="black" stroke-width="3" fill="red" />
</svg>
 </body>
</html>
```
<!DOCTYPE html>
<html>
<body>
<svg height="100" width="100">
 <circle cx="50" cy="50" r="40" stroke="black" stroke-width="3" fill="red" />
</svg>
 </body>
</html>
  
  W tym przypadku mamy np. znacznik np. <*circle>* opisujący parametry koła i który może być  
właściwie zinterpretowany przez dedykowaną aplikację (np. przeglądarki www).  
Jako ciekawostkę można podać fakt, że również pakiet MS Office wykorzystuje format XML do  
przechowywania informacji o dodatkowych parametrach formatowania danych. Na przykład pliki z  
rozszerzeniem docx, to nic innego jak spakowane algorytmem zip katalogi z plikami xml.  

<span style="color:green">$unzip</span> <span style="color:red">-l</span> <span style="color:purple">test</span>.docx  
Archive; <span style="color:purple">test</span>.docx  
 Lenght   <span style="color:fuchsia">Date</span> **Time** Name  
 <span style="color:red"> -------  ----------  ----- ----</span>  
 573 2020-10-11 18:20 _rels/.rels  
 731 2020-10-11 18:20 docProps/core.xml  
 508 2020-10-11 18:20 docProps/app.xml  
 531 2020-10-11 18:20 word/_rels/document.xml.rels  
 1421 2020-10-11 18:20 word/document.xml  
 2429 2020-10-11 18:20 word/styles.xml  
 853 2020-10-11 18:20 word/fontTable.xml  
 241 2020-10-11 18:20 word/settings.xml  
 1374 2020-10-11 18:20 [Content_Types].xml  

Wszystkie te języki znaczników cechują się rozbudowaną i złożoną składnią i dlatego do ich edycji  
wymagają najczęściej dedykowanych narzędzi w postaci specjalizowanych edytorów. By  
wyeliminować powyższą niedogodność powstał **Markdown** - uproszczony język znaczników  
służący do formatowania dokumentów tekstowych (bez konieczności używania specjalizowanych  
narzędzi). Dokumenty w tym formacie można bardzo łatwo konwertować do wielu innych  
formatów: np. html, pdf, ps (postscript), epub, xml i wiele innych. Format ten jest powszechnie  
używany do tworzenia plików README .md (w projektach open source) i powszechnie  
obsługiwany przez serwery git’a. Język ten został stworzony w 2004 r. a jego twórcami byli John  
Gruber i Aaron Swartz. W kolejnych latach podjęto prace w celu stworzenia standardu rozwiązania  
i tak w 2016 r. opublikowano dokument [RFC 7764](https://datatracker.ietf.org/doc/html/rfc7764) który zawiera opis kilku odmian tegoż języka:  
* CommonMark,
* GitHub Flavored Markdown (GFM),
* Markdown Extra.
* ##  3. <a name='Podstawyskadni'></a>Podstawy składni
Podany link: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet zawiera opis  
podstawowych elementów składni w języku angielskim. Poniżej zostanie przedstawiony ich krótki  
opis w języku polskim.  
##  4. <a name='Definiowanienagwkw'></a>Definiowanie nagłówków
W tym celu używamy znaków kratki: #  
Lewe okno zawiera kod źródłowy – prawe -podgląd przetworzonego tekstu  
![zdj3](./zdj3.JPG)  
##  5. <a name='Definiowanielist'></a>Definiowanie list
![zdj4](./zdj4.JPG)  
Listy numerowane definiujemy wstawiając numery kolejnych pozycji zakończone kropką.  
Listy nienumerowane definiujemy znakami: *,+,-  
##  6. <a name='Wyrnianietekstu'></a>Wyróżnianie tekstu
![zdj5](./zdj5.JPG)  
##  7. <a name='Tabele'></a>Tabele
![zdj6](./zdj6.JPG)  
Centrowanie zawartości kolumn realizowane jest poprzez odpowiednie użycie znaku dwukropka:  
##  8. <a name='Odnonikidozasobw'></a>Odnośniki do zasobów
\[odnośnik do zasobów](www.gazeta.pl)  
<p>[odnośnik do pliku](LICENSE.md)</p>  

[odnośnik do kolejnego zasobu][1]  
[1]: [http://google,com](http://google.com)  
##  9. <a name='Obrazki'></a>Obrazki
\![alt text]\(https://server.com/images/icon48.png "Logo 1") – obrazek z zasobów  
internetowych  
\![]\(logo.png) – obraz z lokalnych zasobów  
##  10. <a name='Kodrdowydlarnychjzykwprogramowania'></a>Kod źródłowy dla różnych języków programowania 
![zdj7](./zdj7.JPG)
##  11. <a name='Tworzeniespisutrecinapodstawienagwkw'></a>Tworzenie spisu treści na podstawie nagłówków
![zdj8](./zdj8.JPG)  
##  12. <a name='Edytorydedykowane'></a>Edytory dedykowane
Pracę nad dokumentami w formacie Markdown( rozszerzenie md) można wykonywać w  
dowolnym edytorze tekstowym. Aczkolwiek istnieje wiele dedykowanych narzędzi:  
   1. Edytor Typora - https://typora.io/
   2. Visual Studio Code z wtyczką „markdown preview”

 ![zdj9](./zdj9.JPG)  
 ##  13. <a name='PandocsystemdokonwersjidokumentwMarkdowndoinnychformatw'></a>Pandoc – system do konwersji dokumentów Markdown do innych formatóww
 Jest oprogramowanie typu open source służące do konwertowania dokumentów pomiędzy  
różnymi formatami.  
Pod poniższym linkiem można obejrzeć przykłady użycia:  
https://pandoc.org/demos.html  
Oprogramowanie to można pobrać z spod adresu: https://pandoc.org/installing.html  
Jeżeli chcemy konwertować do formatu latex i pdf trzeba doinstalować oprogramowanie  
składu *Latex* (np. Na MS Windows najlepiej sprawdzi się Miktex - https://miktex.org/)  
Gdyby podczas konwersji do formatu pdf pojawił się komunikat o niemożliwości  
znalezienia programu *pdflatex* rozwiązaniem jest wskazanie w zmiennej środowiskowej  
**PATH** miejsca jego położenia  
![zdj10](./zdj10.JPG)  
![zdj11](./zdj11.JPG)  
Pod adresem (https://gitlab.com/mniewins66/templatemn.git) znajduje się przykładowy plik  
Markdown z którego można wygenerować prezentację w formacie *pdf* wykorzystując  
klasę latexa *beamer*.  
W tym celu należy wydać polecenie z poziomu terminala:  
$pandoc templateMN.md -t beamer -o prezentacja.pdf  
